# Planning Your Video

Now that you have your storyboard or at least a clear script, you are ready to begin shooting. StoryMaker will walk you through how to shoot and edit a basic video sequence as well as how to produce more complex stories and stories with narration. (*For more on recording narration, see the narration module).*

---
