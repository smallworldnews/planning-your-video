# Exercises

1) Watch the following video and write out a shot list based on the shot types used to shoot it

*PV_Fig. 2 (let’s pick a  video shot with Storymaker that’s a good example of sequencing. You are more familiar with the content than I)*

2) watch the following video and write a storyboard that either adds shots and interviews or reorganizes existing material to make the story stronger.

*PV_Fig. 3 (let’s pick a  video shot with Storymaker that’s an example of poor shooting and sequencing. You are more familiar with the content than I)*

3) Imagine you have to do a story about a new business that sells electronics. It is a very large store and owned by a German company. Smaller local merchants are concerned the store might put them out of business because they have a bigger selection and can sell imported items at a lower cost.

Storyboard your coverage. Use at least three sequences of five shots each. You may choose to do a voiceover or narration or have only your subjects speak. You need to interview at least three people. Who should they be? What questions should you ask (NOTE: in the storyboard you can put the question where you anticipate the response would go).

---
