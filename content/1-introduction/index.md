# Planning Your Video

### Introduction

Every video story is made up of smaller elements and assembled into a final whole. The smallest element is the individual clip. Understanding the component parts and how they fit together is essential for good video news production. You should plan with these parts in mind.

Not all stories are the same, and not all stories need to use all the same component parts, but all basic news stories will made up of some combination of the parts below:

* **Interviews**

    * The reporter asks subjects or sources questions on camera. The shot is often of the subject, or sometimes the subject and reporter.

* **Voiceover**

    * This can be called a narration. The viewer hears the reporter’s voice while the video illustrates scenes from the issue.

* **Shots (or Visuals)**

    * Shots or visuals go by several names in the news world. They might be called "b-roll" or “clips.” No matter the name, these are shots that show the viewer the scene or depict event the story is about.

* **Sequences**

    * Sequences are groups of shots, typically about five, that are edited together to tell a coherent story. Your story may have many sequences. Sequences are made up of particular types of shots. Shooting for sequences is an important part of the good video work.

* **Reporter Standup**

    * Sometimes called "voice on camera." These are images and audio of the reporter talking to the camera and explaining the story.  Standups are often used to introduce and story and end a story.

    ---
