# 1. Write a Concept Statement

---

This is an easy step to skip, but do not! Taking the time to write a concise statement of your story can save you a lot of trouble later on. It is easy to lose focus when producing a video story. A concept statement will help you stay on track. You can also use it to explain your story to editors or sources. Depending on the story this can be as simple as a paragraph or two or as complex as written rough draft of the story. (*note: you may hear a concept statement sometimes referred to as a "pitch", these are the same thing in principle.*)

Below is the concept statement for [The Way Home: Journey through the Juvenile Justice System](http://www.juvenilejusticejourneys.com/), an award-winning project produced for the NY City News Service. The project is examined more closely in the "Planning your Coverage" module.

"Thousands of young people pass through the New York State juvenile justice system each year. It’s a patchwork of programs and facilities that costs the state and New York City millions of dollars annually. These are the stories of young people’s journeys through what is often a confusing system and their efforts to get back on track."

---
