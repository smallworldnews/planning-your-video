# Making a shot list

This is a shot-by-shot plan for the visual component of your story. Your shot list is organized chronologically and by shot type (*for more on shot types, see the StoryMaker video module*) *and a very brief description on the shot contents. Below are the standard shot types. Good video offers a variety of shot types and shot types appropriate to the information being conveyed*:

<table>
  <tr>
    <td>Establishing shot</td>
    <td>Shows the location / scene of a story. Gives a sense of place.
Typically shot with a wide-angle lens.</td>
  </tr>
  <tr>
    <td>Long shot</td>
    <td>Showcases the characters and how they interact with the location.</td>
  </tr>
  <tr>
    <td>Medium shot</td>
    <td>Directs the audience to focus entirely on one or two characters,
may not provide an understanding of the location.</td>
  </tr>
  <tr>
    <td>Close-up shot</td>
    <td>Forces the audience to focus entirely on a single character,
encourages the emotion of the character.</td>
  </tr>
  <tr>
    <td>Detail shot</td>
    <td>Showcases an interesting detail, often focusing directly on
an important action.</td>
  </tr>
</table>


Here is an example shot list:

![image alt text](image_0.png)

---
