# 5 | Make a Production Plan

Based on your rough outline, make a plan for how you will produce your story. Include locations, dates, and the amount of time you estimate it will take. Contact sources for your story and arrange your interviews. Be realistic about scheduling the elements of your production plan. Can you record your story when you intend to?** **How** about weather? Will there be access issues? Light issues? Be flexible and anticipate changes.

For short news video stories your pre-production work might end here. Based on your script it is time to go out and shoot. Be sure and incorporate the five-shot sequence concept presented below. Stick as close to your script as possible to make editing easier. In post-production you will piece together your final edit based on what you recorded in the field. For more complex stories you might do an additional step of preparation called a storyboard.

---
