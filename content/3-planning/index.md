# Planning

Planning is essential to good video storytelling. Very good video productions progress through five clear stages. Moving from the **concept statement** to **script** to **shot list** to **storyboard** and ending with the **production plan**. For very short piece or for breaking news you may not have the time to write each of these out, but it should become automatic to at least think through these planning elements no matter the video story you are producing. Now we’ll walk through each of these five stages.

---
