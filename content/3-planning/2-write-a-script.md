# 1. Write a script

Producing a script is the next step. This is the first draft of your story. It is the story presented from beginning to end with each scene as an annotated bulletpoint. When writing your script it is important to think through the details of your story as specifically as possible. Try and visualize it from scenes to scene. What information can you provide with the visuals and what needs to be provided by narration or interview?

A time-tested technique to help journalists answer some of the most basic story questions when writing a script is **Five W’s and an H.****[ Rudyard Kiplin**g](http://en.wikipedia.org/wiki/Rudyard_Kipling) made them famous in his "[Just So Stories](http://en.wikipedia.org/wiki/Just_So_Stories)" ([1902](http://en.wikipedia.org/wiki/1902_in_literature)), in which a poem accompanying the tale of "The Elephant's Child" opens with:

*I keep six honest serving-men*

*(They taught me all I knew);*

*Their names are What and Why and When*

*And How and Where and Who.*

<table>
  <tr>
    <td></td>
  </tr>
</table>


This is why the "Five Ws and One H" problem-solving method has been called the "Kipling Method."

Your story should answer each of these questions:

* **When** does the story happen?

* **Who** are the people you want to highlight? Who is important in this shot?

* **What** is the goal of your story? What is important in this shot?

* **Where** is the story taking place?

* **How** did this happen?

* **Why** did it happen?

It is important to remember that this method will provide you only the factual skeleton of your story. Yes, a skeleton is essential but the meat of your story, its flesh and blood, comes from how well you present the fact through good production values, characters that your audience can identify with and a story structure that builds in a compelling way from beginning to middle to end (*see the StoryMaker lesson on story structure*.)

---
