# 4 | Producing a Storyboard

The next pre-production step is your storyboard. A storyboard brings your shot list and script together to wed your images with the audio interviews and narration. A storyboard is an essential piece of the pre-production puzzle for good video. Simply writing out a storyboard will help you build a better story because it will force you to think through scene-to-scene transition, story structure and whether or not you have answered the basic informational questions.

For shorter news pieces or stories for which an exact script or storyboard cannot be planned, the storyboard might be done after the fact and at the beginning of the post-production process. In all cases, a storyboard can help you build a better story when editing in situations where you could not plan for the material you have.

To ensure your storyboard is a detailed roadmap of your story, take note of the essential elements of your story:

1. Note each distinct location in your story with a symbol, such as a star, circle, or square.

2. Note each distinct character in your story with a letter. You may wish to use the first letter of their name, or their initials, to help you remember.

3. Note each unique activity or specific shot you need to capture, use numbers to distinguish actions and images from your characters.

The best way to organize our storyboard is called "the Dual-column Storyboard Format." The dual-column format organizes video and audio instructions into two parallel columns on the page. The video instructions are located on the left side of the page, and the audio instructions are on the right side.

The video instructions are simply your shot list. Some people like to animate the shot list with drawings, but the dual-column format is usually sufficient for news stories.

Audio Instructions must make it clear who is talking. Typically the reporter’s questions or voice over will be in all capital letters. Other sources will be lower case with the subject’s last name before each audio clip to make it clear who is speaking. Avoid hyphenating words at the end of a line and avoid splitting shots at the bottom of the page. Spreading copy out allows for notes and additional instructions to be added during actual production.

<table>
  <tr>
    <td>VIDEO INFORMATION</td>
    <td>AUDIO INFORMATION</td>
  </tr>
  <tr>
    <td>1) Wide Shot - The Detroit River</td>
    <td>1 (Voice Over)FOR GENERATIONS OF IMMIGRANTS, THE DETROIT RIVER WAS AN ARTERY THAT CARRIED RAW IRON ORE FROM THE EAST TO THE AUTOMOBILE FACTORIES ALONG THE RIVER...PULLUTION…KILLED THE FISH" </td>
  </tr>
  <tr>
    <td>2) Close-up - hand putting a hook on a fishing line</td>
    <td>2 (Voice Over) TODAY THE DETROIT RIVER DOES NOT SUPPORT THE INDUSTRY IT ONCE DID BUT FISH ARE RETURNING, INTRODUCING A NEWS INDUSTRY….”</td>
  </tr>
  <tr>
    <td>3) Medium/interview</td>
    <td>3  (Interview) DOUGLAS TAYLOR "I come out each morning … sell to local restaurants…”</td>
  </tr>
</table>


A good storyboard will allow you think through how to organize information to tell the most compelling story. Take note of the essential elements of your story:

1. Note each distinct location in your story with a symbol, such as a star, circle, or square.

2. Note each distinct character in your story with a letter. You may wish to use the first letter of their name, or their initials, to help you remember.

3. Note each unique activity or specific shot you need to capture, use numbers to distinguish actions and images from your characters.

Once you’ve completed a first draft, take a look at your storyboard.

* Have you introduced each character appropriately?

* Be sure to have shots with your characters interacting where appropriate, this drives the narrative and helps the audience understand the relation between characters.

* Have you included action in your shots, and ordered your shots to create a logical narrative?

* Are there other limitations you need to consider, such as time of day? Be sure to take note of these considerations.

---
