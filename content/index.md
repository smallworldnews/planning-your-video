# Planning Your Video

Video is the fastest growing medium on the web. Good video skills are essential to the individual reporter and the social-first news organization.

On a technical level each shot should be well focused and well exposed. Your images should not be shaky -- you must stabilize the camera. Camera movements such as zooms (the shot moves in and out from telephoto to wide) and pans (the camera moves from left to right or right to left) should be smooth and done with purpose.

The best news video goes way beyond the technical. True video storytelling use images and audio to create complex stories that are informative AND emotional. Simply shooting footage and talking to people will not work. You need to have a clear concept and strategy in place before you begin shooting to make really strong stories.

This module will walk you through how to plan a basic news video package. It assumes the use of StoryMaker to shoot and edit your video.

SWN and IWPR hope these modules and the skills they introduce will help journalists and the communities in which they live and work produce exciting stories that embody the highest standards of journalism with the newest tools. Good stories can change the world.

---
