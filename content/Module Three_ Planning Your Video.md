**Social-First ReportingModule Three | Planning your Video**

[[TOC]]

# **Module Introduction**

This module on producing video is one of four modules intended to help start-up and existing news organizations develop the skills required to produce high quality journalism for today’s mobile media landscape.  The four modules, which are accompanied by "Crafting a Social-first News Strategy," an overview set of guidelines on developing an editorial plan, cover the following topics:

* Module One: Planning your Coverage

* Module Two: Photography for News

* Module Three: Planning your Video

* Module Four: Narration for Video and Audio

The modules are design to be hands-on and to teach the core multimedia skills reporters and editors need to possess to do the job in a media environment where people get much of their news through mobile devices and social sites, often contributing content to a story as it develops. This is called  ‘Social-first’ news and is quickly changing how the news is reported and distributed.

Each module in the series has four parts:

I. An Introduction to the topic.

II. A collection of professional examples to illustrate the concept along with additional resources that might be helpful to reporters and the news orgs they work for.

III. Exercises/quizzes that emphasize application of the skill.

IV. Critique of an assignment by a mentor or staff person.

Each module included a package of multimedia example content and is can be completed by reporters, editors and others independently. In total, steps one through three should take between 30 minutes and two hours to complete.  The time required for step four, the assignment, will very.

The modules are intended for news organizations using StoryMaker, a cutting-edge Android based app that helps journalists produce, edit and distribute multimedia content with mobile devices. StoryMaker can be downloaded from the Google Play Store.

The Modules for StoryMaker were produced by Small World News (SWN) for the Institute for War and Peace Reporting (IWPR). Small World News started in 2005 with its first project ‘Alive in Baghdad,’  which initially produced weekly video packages on citizens’ daily life. SWN and the ‘Alive in…’ project has expanded globally and now focuses on creating social-first media projects in emerging markets. SWN has trained hundreds of journalists and activists around the globe, and has produced several media guides, including a guide to safety for reporters and a comprehensive basic journalism course as part of the StoryMaker mobile application.

In 2011 Small World News partnered with the Guardian Project, Free Press Unlimited, and Scal.io, to produce StoryMaker, a secure journalism production and learning tool for journalists using mobile tools. Small World News’s stakeholders are journalists, activists, and citizens anywhere in the world working to tell stories better.

IWPR gives voice to people at the frontlines of conflict, crisis and change. From Afghanistan to Zimbabwe, IWPR helps people in the world's most challenging environments have the information they need to drive positive changes in their lives — holding government to account, demanding constructive solutions, strengthening civil society and securing human rights. Amid war, dictatorship, and political transition, IWPR forges the skills and capacity of local journalism, strengthens local media institutions and engages with civil society and governments to ensure that information achieves impact.

SWN and IWPR hope these modules and the skills they introduce will help journalists and the communities in which they live and work produce exciting stories that embody the highest standards of journalism with the newest tools. Good stories can change the world.

* * *


# Module Three: Planning Your Video

* * *


## Part One | Overview

Video is the fastest growing medium on the web. Good video skills are essential to the individual reporter and the social-first news organization.

On a technical level each shot should be well focused and well exposed. Your images should not be shaky -- you must stabilize the camera. Camera movements such as zooms (the shot moves in and out from telephoto to wide) and pans (the camera moves from left to right or right to left) should be smooth and done with purpose.

The best news video goes way beyond the technical. True video storytelling use images and audio to create complex stories that are informative AND emotional. Simply shooting footage and talking to people will not work. You need to have a clear concept and strategy in place before you begin shooting to make really strong stories.

This module will walk you through how to plan a basic news video package. It assumes the use of StoryMaker to shoot and edit your video.

### Introduction

Every video story is made up of smaller elements and assembled into a final whole. The smallest element is the individual clip. Understanding the component parts and how they fit together is essential for good video news production. You should plan with these parts in mind.

Not all stories are the same, and not all stories need to use all the same component parts, but all basic news stories will made up of some combination of the parts below:

* **Interviews**

    * The reporter asks subjects or sources questions on camera. The shot is often of the subject, or sometimes the subject and reporter.

* **Voiceover**

    * This can be called a narration. The viewer hears the reporter’s voice while the video illustrates scenes from the issue.

* **Shots (or Visuals)**

    * Shots or visuals go by several names in the news world. They might be called "b-roll" or “clips.” No matter the name, these are shots that show the viewer the scene or depict event the story is about.

* **Sequences**

    * Sequences are groups of shots, typically about five, that are edited together to tell a coherent story. Your story may have many sequences. Sequences are made up of particular types of shots. Shooting for sequences is an important part of the good video work.

* **Reporter Standup**

    * Sometimes called "voice on camera." These are images and audio of the reporter talking to the camera and explaining the story.  Standups are often used to introduce and story and end a story.

### Research

Your work on a story begins with research. That research starts by asking yourself a series of questions:

**Who is the Audience?**

Do you know whom you want to speak to? Knowing whom you want to reach with your story will determine how you tell it. See the "Planning Your Coverage" module for more about understanding your audience.

**What is Your Message?**

What's the one idea you want to communicate to your audience? What do you want the viewer to learn? What larger issue does the story address? How does the story fit into your news organizations editorial focus? What do you want viewers to do as a result of having watched the video?

**What are the basic facts of my story?**

Spend time with people involved. Follow your curiosity. Ask yourself questions like:

* What have I not yet been told about this subject?

* Is everything I have been told the truth? How much do I need to verify?

* What would I personally like to know about this subject?

* If I were a member of the audience, what would I want to learn about this subject?

* What can I find that is little known on this subject?

* If the shooting has not yet started, what information can I gather that would aid the filming process?

**How do I best tell my story?**

Video stories need to be crafted so that they are interesting to your audience. How can I illustrate the issue in a manner people will identify with the issue? What characters or sources can I use that my audience will relate to? How can I structure my story to create dramatic tension as it builds toward the conclusion?

### Planning

Planning is essential to good video storytelling. Very good video productions progress through five clear stages. Moving from the **concept statement** to **script** to **shot list** to **storyboard** and ending with the **production plan**. For very short piece or for breaking news you may not have the time to write each of these out, but it should become automatic to at least think through these planning elements no matter the video story you are producing. Now we’ll walk through each of these five stages.

#### 1 | Write a concept statement

This is an easy step to skip, but do not! Taking the time to write a concise statement of your story can save you a lot of trouble later on. It is easy to lose focus when producing a video story. A concept statement will help you stay on track. You can also use it to explain your story to editors or sources. Depending on the story this can be as simple as a paragraph or two or as complex as written rough draft of the story. (*note: you may hear a concept statement sometimes referred to as a "pitch", these are the same thing in principle.*)

Below is the concept statement for [The Way Home: Journey through the Juvenile Justice System](http://www.juvenilejusticejourneys.com/), an award-winning project produced for the NY City News Service. The project is examined more closely in the "Planning your Coverage" module.

"Thousands of young people pass through the New York State juvenile justice system each year. It’s a patchwork of programs and facilities that costs the state and New York City millions of dollars annually. These are the stories of young people’s journeys through what is often a confusing system and their efforts to get back on track."

#### 2 | Write a script

Producing a script is the next step. This is the first draft of your story. It is the story presented from beginning to end with each scene as an annotated bulletpoint. When writing your script it is important to think through the details of your story as specifically as possible. Try and visualize it from scenes to scene. What information can you provide with the visuals and what needs to be provided by narration or interview?

A time-tested technique to help journalists answer some of the most basic story questions when writing a script is **Five W’s and an H.****[ Rudyard Kiplin**g](http://en.wikipedia.org/wiki/Rudyard_Kipling) made them famous in his "[Just So Stories](http://en.wikipedia.org/wiki/Just_So_Stories)" ([1902](http://en.wikipedia.org/wiki/1902_in_literature)), in which a poem accompanying the tale of "The Elephant's Child" opens with:

*I keep six honest serving-men*

*(They taught me all I knew);*

*Their names are What and Why and When*

*And How and Where and Who.*

<table>
  <tr>
    <td></td>
  </tr>
</table>


This is why the "Five Ws and One H" problem-solving method has been called the "Kipling Method." Your story should answer each of these questions:

* **When** does the story happen?

* **Who **are the people you want to highlight? Who is important in this shot?

* **What **is the goal of your story? What is important in this shot?

* **Where **is the story taking place?

* **How **did this happen?

* **Why **did it happen?

It is important to remember that this method will provide you only the factual skeleton of your story. Yes, a skeleton is essential but the meat of your story, its flesh and blood, comes from how well you present the fact through good production values, characters that your audience can identify with and a story structure that builds in a compelling way from beginning to middle to end (*see the StoryMaker lesson on story structure*.)

#### 3 | Making a shot list

This is a shot-by-shot plan for the visual component of your story. Your shot list is organized chronologically and by shot type (*for more on shot types, see the StoryMaker video module) *and a very brief description on the shot contents. Below are the standard shot types. Good video offers a variety of shot types and shot types appropriate to the information being conveyed:

<table>
  <tr>
    <td>Establishing shot</td>
    <td>Shows the location / scene of a story. Gives a sense of place. Typically shot with a wide-angle lens.</td>
  </tr>
  <tr>
    <td>Long shot</td>
    <td>Showcases the characters and how they interact with the location.</td>
  </tr>
  <tr>
    <td>Medium shot</td>
    <td>Directs the audience to focus entirely on one or two characters, may not provide an understanding of the location.</td>
  </tr>
  <tr>
    <td>Close-up shot</td>
    <td>Forces the audience to focus entirely on a single character,
encourages the emotion of the character.</td>
  </tr>
  <tr>
    <td>Detail shot</td>
    <td>Showcases an interesting detail, often focusing directly on an important action.</td>
  </tr>
</table>


Here is an example shot list:

![image alt text](image_0.png)

#### 4 | Producing a Storyboard

The next pre-production step is your storyboard. A storyboard brings your shot list and script together to wed your images with the audio interviews and narration. A storyboard is an essential piece of the pre-production puzzle for good video. Simply writing out a storyboard will help you build a better story because it will force you to think through scene-to-scene transition, story structure and whether or not you have answered the basic informational questions.

For shorter news pieces or stories for which an exact script or storyboard cannot be planned, the storyboard might be done after the fact and at the beginning of the post-production process. In all cases, a storyboard can help you build a better story when editing in situations where you could not plan for the material you have.

To ensure your storyboard is a detailed roadmap of your story, take note of the essential elements of your story:

1. Note each distinct location in your story with a symbol, such as a star, circle, or square.

2. Note each distinct character in your story with a letter. You may wish to use the first letter of their name, or their initials, to help you remember.

3. Note each unique activity or specific shot you need to capture, use numbers to distinguish actions and images from your characters.

The best way to organize our storyboard is called "the Dual-column Storyboard Format." The dual-column format organizes video and audio instructions into two parallel columns on the page. The video instructions are located on the left side of the page, and the audio instructions are on the right side.

The video instructions are simply your shot list. Some people like to animate the shot list with drawings, but the dual-column format is usually sufficient for news stories.

Audio Instructions must make it clear who is talking. Typically the reporter’s questions or voice over will be in all capital letters. Other sources will be lower case with the subject’s last name before each audio clip to make it clear who is speaking. Avoid hyphenating words at the end of a line and avoid splitting shots at the bottom of the page. Spreading copy out allows for notes and additional instructions to be added during actual production.

<table>
  <tr>
    <td>VIDEO INFORMATION</td>
    <td>AUDIO INFORMATION</td>
  </tr>
  <tr>
    <td>1) Wide Shot - The Detroit River</td>
    <td>1 (Voice Over)FOR GENERATIONS OF IMMIGRANTS, THE DETROIT RIVER WAS AN ARTERY THAT CARRIED RAW IRON ORE FROM THE EAST TO THE AUTOMOBILE FACTORIES ALONG THE RIVER...PULLUTION…KILLED THE FISH" </td>
  </tr>
  <tr>
    <td>2) Close-up - hand putting a hook on a fishing line</td>
    <td>2 (Voice Over) TODAY THE DETROIT RIVER DOES NOT SUPPORT THE INDUSTRY IT ONCE DID BUT FISH ARE RETURNING, INTRODUCING A NEWS INDUSTRY….”</td>
  </tr>
  <tr>
    <td>3) Medium/interview</td>
    <td>3  (Interview) DOUGLAS TAYLOR "I come out each morning … sell to local restaurants…”</td>
  </tr>
</table>


A good storyboard will allow you think through how to organize information to tell the most compelling story. Take note of the essential elements of your story:

1. Note each distinct location in your story with a symbol, such as a star, circle, or square.

2. Note each distinct character in your story with a letter. You may wish to use the first letter of their name, or their initials, to help you remember.

3. Note each unique activity or specific shot you need to capture, use numbers to distinguish actions and images from your characters.

Once you’ve completed a first draft, take a look at your storyboard.

* Have you introduced each character appropriately?

* Be sure to have shots with your characters interacting where appropriate, this drives the narrative and helps the audience understand the relation between characters.

* Have you included action in your shots, and ordered your shots to create a logical narrative?

* Are there other limitations you need to consider, such as time of day? Be sure to take note of these considerations.

#### 5 | Make a Production Plan

Based on your rough outline, make a plan for how you will produce your story. Include locations, dates, and the amount of time you estimate it will take. Contact sources for your story and arrange your interviews. Be realistic about scheduling the elements of your production plan. Can you record your story when you intend to?** **How about weather? Will there be access issues? Light issues? Be flexible and anticipate changes.

For short news video stories your pre-production work might end here. Based on your script it is time to go out and shoot. Be sure and incorporate the five-shot sequence concept presented below. Stick as close to your script as possible to make editing easier. In post-production you will piece together your final edit based on what you recorded in the field. For more complex stories you might do an additional step of preparation called a storyboard.

### Producing your Video

Now that you have your storyboard or at least a clear script, you are ready to begin shooting. StoryMaker will walk you through how to shoot and edit a basic video sequence as well as how to produce more complex stories and stories with narration. (*For more on recording narration, see the narration module).*

## Part Two | Examples

### Sample Script

*PV_fig. 1 (Grafitti Story)*



Community responds to proposed tearing down of graffiti mecca five pointz

<table>
  <tr>
    <td>




TS of spray pain can being shaken
MS of graffiti artist shaking spray paint can, graffiti wall in front of him.
MS spray painting on wall








Interview shot with 5 Pointz volunteer, Javier Rivera

WS of graffiti artist spray painting on wall, make a symbol-like circle.

TS of hand pressing down on spray paint can, releasing puffs of pink pain


WS of 5 Pointz, main open court

WS of graffiti wall, ‘I heart NY’

CU of graffiti work, same wall

WS of another graffiti wall on other side of court

MS of photographer taking photo of graffiti artist at work


MS of photographer taking photos of graffiti near subway track

MS of photographer taking photo against graffiti wall used as backdrop of interview with Evans


MS Evans interview shot

MS of guy turning in circle with camera

MS of Kanellos working in diner, grabbing food from kitchen window

MS interview shot of Kanellos, sitting in booth. 5 Pointz walls outside

MS of girl doing hand stand in front of graffiti wall

MS interview shot of Kanellos

TS Dead End sign
WS Dead end sign, subway line and 5 Pointz in background
MS of Citi Bank
WS of Citi bank building in background with 5 Pointz in foreground

MS interview shot of Markman
Markman flipping through photographs of 5 Pointz on camera
MS interview shot of Markman


TS looking up at subway tracks. Train enters frame.
WS head on shot of train rounding the corner away from 5Pointz. Graffiti on subway structure visible.
CU of train pulling away, 7 sign

Interview shot of Evans




MS exterior of diner from 5Pointz, people and cars passing by
MS of car driving down street

Interview shot of Kanellos




MS of Kanellos tidying work station

Interview shot of Kanellos




MS of Rivera explain artwork to tourist



Interview shot of Rivera
MS of graffiti artist putting final touches on work
MS of graffiti artist taking photo of his work
Interview shot of Rivera

MS of Rivera and graffiti artist looking at camera, talking

Interview shot with Javier

WS of "Welcome to 5Pointz" door
CU of running fan above door and artwork
MS of someone opening “Welcome to 5 Pointz” door and closing it.
 </td>
    <td></td>
    <td>Anchor Lead: Plans to knock down graffiti mecca 5 Pointz in Long Island City has local residents and business owners talking about gentrification. Aine Pennello reports.


NAT SOUND of spray paint can being shaken




NAT SOUND of spray painting

RIVERA: Graffiti’s been around since the cavemen.

NAT SOUND of spray painting

RIVERA: Doodling on cave walls.


Native Americans, the pyramids, the Egyptians.



It’s all a form of graf because you’re telling your stories through symbols.


NAT SOUND of spray paint




UNLIKE THE ART OF GRAFFITI, 5 POINTZ IS RELATIVELY NEW.

SINCE OFFERING ITS WALLS TO

GRAFFITI ARTISTS IN 2001,


THE BUILDING HAS BECOME THE LARGEST PLACE IN THE WORLD WHERE ARTISTS


CAN LEGALLY SPRAY PAINT.

NAT SOUND of photo click and spray paint


AND RESIDENTS SAY IT’S BECOMING A PERMANENT

FIXTURE IN LONG ISLAND CITY.






EVANS: You’ll see packs of tourists come over here by a busload. You can’t help but notice it, you know.
It’s part of the neighborhood.

BUT LOCAL DINER MANAGER,
NICK KANELLOS, SAYS 5 POINTZ BRINGS IN MORE THAN JUST TOURISTS.

KANELLOS: It generates business, it generates activity in the area



and it brings life. Instead of having, you know, big buildings that

after six o’clock – click – they shut down and there’s nobody around.

BUT THE OWNER OF THE BUILDING HAS PLANS TO REPLACE 5 POINTZ WITH TWO LUXURY APARTMENT TOWERS AND A SHOPPING MALL, ONE OF THE LATEST GENTRIFICATION PROJECTS IN A NEIGHBORHOOD
KNOWN AS THE SECOND MANHATTAN.



NEIL MARKMAN: You know, I came to New York because I thought it was a place
for expression and I thought it was a place that was much different than any other city. And with all the

gentrification it just feels like it’s becoming like any other large city or any mall in America. (sound of train in background)

NAT sound of train

A TREND THAT’S CAUSING A DIVIDE BETWEEN LOCAL OLD-TIMERS


AND INCOMING RESIDENTS.


EVANS: We, of the local residents don’t have much interaction with them of the big condos. They don’t even recognize the neighborhood really. It’s just a place that they pass through to get to their condo.

GENTIRIFICATION CAN ALSO LEAD TO MORE PRACTICAL ISSUES. KANELLOS SAYS

HE SPENDS 30 TO 40 MINUTES CIRCLING THE BLOCK EVERY DAY TO FIND PARKING.

KANELLOS: Everybody wants to put up towers but nobody’s thinking about people driving. So there’s been less and less and less and eventually it’s going to be like the city and no one can come over here with a car.

BUT HE ADMITS THAT GENTRIFICATION IS A MIXTURE OF GOOD AND BAD.

KANELLOS: Yes, it has helped the area, all this developments and what they do. On the other hand, it’s taking the Long Island City as a neighborhood away and making it a semi-commercial area.

5POINTZ VOLUNTEER, JAVIER RIVERA, SAYS MUCH LIKE THE BUILDINGS IN THE NEIGHBORHOOD, GRAFFITI IS TEMPORARY.



RIVERA: I’ve seen stuff here last not even 20 minutes.
You do something, you get your photograph, your little video,
and the minute  you go up to that corner and you leave that dead end,

the wall is given to another artists.

SO WHAT WOULD THE LOSS OF 5 POINTZ MEAN TO GRAFITTI ARTISTS LIKE RIVERA?

RIVERA: It would hurt,
it would hurt. But I’m a New Yorker born and raised. I’ve seen buildings come and go, I’ve had
careers come and go. So, I will adjust.

I will find another

something to, you know, occupy my time until I no longer walk this earth.


AINE PENNELLO, NEW YORK</td>
  </tr>
</table>


## Part Three | Exercises

1) Watch the following video and write out a shot list based on the shot types used to shoot it

*PV_Fig. 2 (let’s pick a  video shot with Storymaker that’s a good example of sequencing. You are more familiar with the content than I)*

2) watch the following video and write a storyboard that either adds shots and interviews or reorganizes existing material to make the story stronger.

*PV_Fig. 3 (let’s pick a  video shot with Storymaker that’s an example of poor shooting and sequencing. You are more familiar with the content than I)*

3) Imagine you have to do a story about a new business that sells electronics. It is a very large store and owned by a German company. Smaller local merchants are concerned the store might put them out of business because they have a bigger selection and can sell imported items at a lower cost.

Storyboard your coverage. Use at least three sequences of five shots each. You may choose to do a voiceover or narration or have only your subjects speak. You need to interview at least three people. Who should they be? What questions should you ask (NOTE: in the storyboard you can put the question where you anticipate the response would go).

## Part Four | Assignment

Work with an editor or mentor to plan a story using each of the steps outlined here. Did the process make your story stronger? How might you have better used the steps outlined here?
