# Research

Your work on a story begins with research. That research starts by asking yourself a series of questions:

**Who is the Audience?**

Do you know whom you want to speak to? Knowing whom you want to reach with your story will determine how you tell it. See the "Planning Your Coverage" module for more about understanding your audience.

**What is Your Message?**

What's the one idea you want to communicate to your audience? What do you want the viewer to learn? What larger issue does the story address? How does the story fit into your news organizations editorial focus? What do you want viewers to do as a result of having watched the video?

**What are the basic facts of my story?**

Spend time with people involved. Follow your curiosity. Ask yourself questions like:

* What have I not yet been told about this subject?

* Is everything I have been told the truth? How much do I need to verify?

* What would I personally like to know about this subject?

* If I were a member of the audience, what would I want to learn about this subject?

* What can I find that is little known on this subject?

* If the shooting has not yet started, what information can I gather that would aid the filming process?

**How do I best tell my story?**

Video stories need to be crafted so that they are interesting to your audience. How can I illustrate the issue in a manner people will identify with the issue? What characters or sources can I use that my audience will relate to? How can I structure my story to create dramatic tension as it builds toward the conclusion?

---
