# Module Introduction

This module on producing video is one of four modules intended to help start-up and existing news organizations develop the skills required to produce high quality journalism for today’s mobile media landscape.  The four modules, which are accompanied by "Crafting a Social-first News Strategy," an overview set of guidelines on developing an editorial plan, cover the following topics:

* Module One: Planning your Coverage

* Module Two: Photography for News

* Module Three: Planning your Video

* Module Four: Narration for Video and Audio

The modules are design to be hands-on and to teach the core multimedia skills reporters and editors need to possess to do the job in a media environment where people get much of their news through mobile devices and social sites, often contributing content to a story as it develops. This is called  ‘Social-first’ news and is quickly changing how the news is reported and distributed.

Each module in the series has four parts:

I. An Introduction to the topic.

II. A collection of professional examples to illustrate the concept along with additional resources that might be helpful to reporters and the news orgs they work for.

III. Exercises/quizzes that emphasize application of the skill.

IV. Critique of an assignment by a mentor or staff person.

Each module included a package of multimedia example content and is can be completed by reporters, editors and others independently. In total, steps one through three should take between 30 minutes and two hours to complete.  The time required for step four, the assignment, will very.

The modules are intended for news organizations using StoryMaker, a cutting-edge Android based app that helps journalists produce, edit and distribute multimedia content with mobile devices. StoryMaker can be downloaded from the Google Play Store.

The Modules for StoryMaker were produced by Small World News (SWN) for the Institute for War and Peace Reporting (IWPR). Small World News started in 2005 with its first project ‘Alive in Baghdad,’  which initially produced weekly video packages on citizens’ daily life. SWN and the ‘Alive in…’ project has expanded globally and now focuses on creating social-first media projects in emerging markets. SWN has trained hundreds of journalists and activists around the globe, and has produced several media guides, including a guide to safety for reporters and a comprehensive basic journalism course as part of the StoryMaker mobile application.

In 2011 Small World News partnered with the Guardian Project, Free Press Unlimited, and Scal.io, to produce StoryMaker, a secure journalism production and learning tool for journalists using mobile tools. Small World News’s stakeholders are journalists, activists, and citizens anywhere in the world working to tell stories better.

IWPR gives voice to people at the frontlines of conflict, crisis and change. From Afghanistan to Zimbabwe, IWPR helps people in the world's most challenging environments have the information they need to drive positive changes in their lives — holding government to account, demanding constructive solutions, strengthening civil society and securing human rights. Amid war, dictatorship, and political transition, IWPR forges the skills and capacity of local journalism, strengthens local media institutions and engages with civil society and governments to ensure that information achieves impact.


SWN and IWPR hope these modules and the skills they introduce will help journalists and the communities in which they live and work produce exciting stories that embody the highest standards of journalism with the newest tools. Good stories can change the world.

---
